package com.vazhasapp.assignment13

import android.util.Log.d
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.vazhasapp.assignment13.api.NewsApiService
import com.vazhasapp.assignment13.model.News
import java.lang.Exception


private const val FIRST_PAGE = 1

class Paging(val news: NewsApiService) : PagingSource<Int, News>() {

    override fun getRefreshKey(state: PagingState<Int, News>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, News> {
        return try {
            val page = params.key ?: FIRST_PAGE
            val response = news.getAllNews().getNews()

            LoadResult.Page(
                data = response.body() ?: emptyList(),
                prevKey = if (page == FIRST_PAGE) null else page,
                nextKey = if (response.body()!!.isEmpty()) null else page + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}