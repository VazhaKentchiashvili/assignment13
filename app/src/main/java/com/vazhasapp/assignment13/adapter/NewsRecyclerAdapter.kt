package com.vazhasapp.assignment13.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.vazhasapp.assignment13.databinding.NewsItemCardViewBinding
import com.vazhasapp.assignment13.model.News

class NewsRecyclerAdapter : PagingDataAdapter<News, NewsRecyclerAdapter.NewsRecyclerViewHolder>(
    NEWS_COMPARATOR
) {

    private val newsList = mutableListOf<News>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsRecyclerViewHolder {
        return NewsRecyclerViewHolder(
            NewsItemCardViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NewsRecyclerViewHolder, position: Int) {
        holder.bind()
    }

    inner class NewsRecyclerViewHolder(private val binding: NewsItemCardViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var currentNews: News

        fun bind() {
            currentNews = getItem(absoluteAdapterPosition)!!

            binding.newsUpdatedAtTextView.text = currentNews.updatedAt.toString()
            binding.titleTextView.text = currentNews.titleKA
            Glide.with(binding.newsCoverImageView.context).load(currentNews.cover)
                .into(binding.newsCoverImageView)

        }
    }

    companion object {
        private val NEWS_COMPARATOR = object : DiffUtil.ItemCallback<News>() {
            override fun areItemsTheSame(oldItem: News, newItem: News) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: News, newItem: News) =
                oldItem == newItem
        }
    }

    fun setData(newsList: MutableList<News>) {
        this.newsList.clear()
        this.newsList.addAll(newsList)
        notifyDataSetChanged()
    }

    fun clearData() {
        newsList.clear()
        notifyDataSetChanged()
    }
}

