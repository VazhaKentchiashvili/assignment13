package com.vazhasapp.assignment13.api

import com.vazhasapp.assignment13.api.NewsApiService.END_POINT
import com.vazhasapp.assignment13.model.News
import retrofit2.Response
import retrofit2.http.GET

interface NewsApi {

    @GET(END_POINT)
    suspend fun getNews(): Response<List<News>>

}