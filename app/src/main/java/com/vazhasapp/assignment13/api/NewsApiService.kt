package com.vazhasapp.assignment13.api

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.vazhasapp.assignment13.Paging
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NewsApiService {
    private const val BASE_URL = "http://139.162.207.17/"
    const val END_POINT = "api/m/v2/news"

    fun getAllNews(): NewsApi {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NewsApi::class.java)
    }

    fun getNewPageResult() = Pager(
        config = PagingConfig(
            pageSize = 10,
            enablePlaceholders = false,
        ),
        pagingSourceFactory = { Paging(NewsApiService) }
    ).liveData
}