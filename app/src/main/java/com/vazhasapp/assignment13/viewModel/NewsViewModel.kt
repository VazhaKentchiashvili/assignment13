package com.vazhasapp.assignment13.viewModel

import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.vazhasapp.assignment13.ResponseHandler
import com.vazhasapp.assignment13.api.NewsApiService
import com.vazhasapp.assignment13.model.News
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class NewsViewModel : ViewModel() {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    private var _newsLiveData = MutableLiveData<ResponseHandler<List<News>>>()
    val newsLiveData: LiveData<ResponseHandler<List<News>>> = _newsLiveData

    private var _newGeneratedNews = MutableLiveData(1)

    val newGeneratedNews = _newGeneratedNews.switchMap {
        NewsApiService.getNewPageResult().cachedIn(viewModelScope)
    }

    fun init() {
        CoroutineScope(ioDispatcher).launch {
            getNewsResponse()
        }
    }

    private suspend fun getNewsResponse() {
        val result = NewsApiService.getAllNews().getNews()

        _newsLiveData.postValue(ResponseHandler.Loading(true))
        if (result.isSuccessful) {
            _newsLiveData.postValue(ResponseHandler.Success(result.body()))
            _newsLiveData.postValue(ResponseHandler.Loading(false))
        } else {
            result.code()
            ResponseHandler.Loading(false)
            ResponseHandler.Error("${result.errorBody()}")
        }
    }
}